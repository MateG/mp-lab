#include <stdlib.h>
#include <stdio.h>
#include "mrepro.h"

#define USAGE "Usage: %s [-t tcp_port] [-u udp_port] [-p popis]"
#define STDIN 0

int handleStdinCommand(char* payload);
void handleTCPRequest(int socket, char* payload);
void handleUDPRequest(int socket, char* payload);
void handleCommand(char* command, char* payload, int socket,
    struct sockaddr* from, socklen_t from_length);

#define DEFAULT_PORT "1234"
#define DEFAULT_PAYLOAD ""
#define TCP_BACKLOG 10
int main(int argc, char** argv) {
    char* tcp_port = DEFAULT_PORT;
    char* udp_port = DEFAULT_PORT;
    char payload[1024];
    int ch;
    int tcp_socket;
    int udp_socket;
    fd_set read_fds;

    memcpy(payload, DEFAULT_PAYLOAD, strlen(DEFAULT_PAYLOAD)+1);
    while ((ch = getopt(argc, argv, "t:u:p:")) != -1) {
        switch (ch) {
            case 't': tcp_port = optarg; break;
            case 'u': udp_port = optarg; break;
            case 'p': memcpy(payload, optarg, strlen(optarg)+1); break;
            default: errx(1, USAGE, argv[0]); return 1;
        }
    }

    ReuseNextSocketAddress();
    tcp_socket = TCPServerOnPort(tcp_port, TCP_BACKLOG);
    udp_socket = UDPServerOnPort(udp_port);

    while (1) {
        FD_ZERO(&read_fds);
        FD_SET(STDIN, &read_fds);
        FD_SET(tcp_socket, &read_fds);
        FD_SET(udp_socket, &read_fds);
        SelectBlock(udp_socket+1, &read_fds, NULL, NULL);
        if (FD_ISSET(STDIN, &read_fds)) {
            if (handleStdinCommand(payload)) break;
        } else if (FD_ISSET(tcp_socket, &read_fds)) {
            handleTCPRequest(tcp_socket, payload);
        } else if (FD_ISSET(udp_socket, &read_fds)) {
            handleUDPRequest(udp_socket, payload);
        }
    }

    Close(tcp_socket);
    Close(udp_socket);
    return 0;
}

#define PRINT "PRINT"
#define SET "SET "
#define QUIT "QUIT"
int handleStdinCommand(char* payload) {
    char message[1029];
    int message_length;

    message_length = Read(STDIN, message, sizeof(message));
    message[message_length] = '\0';
    if (message[message_length-1] == '\n') {
        message[message_length-1] = '\0';
    }

    if (strcmp(message, PRINT) == 0) {
        printf("%s\n", payload);
    } else if (StartsWith(message, SET)) {
        memcpy(payload, message+strlen(SET),
            strlen(message)-strlen(SET)+1);
    } else if (strcmp(message, QUIT) == 0) {
        return 1;
    } else {
        printf("Ignoring invalid command: %s\n", message);
    }
    return 0;
}
#undef PRINT
#undef SET
#undef QUIT

#define RECEIVED_SIZE 7
void handleTCPRequest(int socket, char* payload) {
    char received[RECEIVED_SIZE];
    int read_length;
    struct sockaddr_in from;
    socklen_t from_length = sizeof(from);

    int new_fd;
    new_fd = Accept(socket, (struct sockaddr*) &from, &from_length);
    read_length = ReadUntil('\n', 1, new_fd, received, sizeof(received));
    received[read_length-1] = '\0';
    handleCommand(received, payload, new_fd,
        (struct sockaddr*) &from, from_length);
    Close(new_fd);
}

void handleUDPRequest(int socket, char* payload) {
    char received[RECEIVED_SIZE];
    int read_length;
    struct sockaddr_in from;
    socklen_t from_length = sizeof(from);

    read_length = RecvFrom(socket, received, sizeof(received),
        (struct sockaddr*) &from, &from_length);
    received[read_length] = '\0';
    if (received[read_length-1] == '\n') received[read_length-1] = '\0';
    handleCommand(received, payload, socket,
        (struct sockaddr*) &from, from_length);
}
#undef RECEIVED_SIZE

#define HELLO "HELLO"
void handleCommand(char* command, char* payload, int socket,
        struct sockaddr* from, socklen_t from_length) {
    char* output;

    if (strcmp(command, HELLO) != 0) {
        printf("Ignoring invalid command: %s\n", command);
        return;
    }

    output = (char*) malloc((strlen(payload)+1) * sizeof(char));
    memcpy(output, payload, strlen(payload));
    output[strlen(payload)] = '\n';
    printf("Sending %s\n", payload);
    SendTo(socket, output, strlen(payload)+1, from, from_length);
    free(output);
}
#undef HELLO
