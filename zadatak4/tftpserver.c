#include <stdlib.h>
#include <stdio.h>
#include "mrepro.h"
#include "message.h"

#define USAGE "Usage: %s [-d] port_name_or_number"
#define PNAME "mg49786:MrePro"

void listenForRequests(char* service_name, int daemon);
void handleRequest();
char* parseNameAndMode(int socket, struct sockaddr_in* from,
    socklen_t from_length, char* name_and_mode, int length);
void transferFile(int socket, FILE* fd, struct sockaddr_in* from,
    socklen_t from_length, int octet);
void logRequest(char* ip, char* filename);
void sendError(int fd, const struct sockaddr* to, socklen_t to_length,
    int code, char* message); 

extern int is_daemon;

int main(int argc, char** argv) {
    int ch;
    int daemon_option = 0;

    while ((ch = getopt(argc, argv, "d")) != -1) {
        switch (ch) {
            case 'd': daemon_option = 1; break;
            default: errx(1, USAGE, argv[0]); return 1;
        }
    }
    if (optind+1 != argc) {
        errx(1, USAGE, argv[0]);
        return 1;
    }

    listenForRequests(argv[optind], daemon_option);
    return 0;
}

void logRequest(char* ip, char* filename) {
    printf("%s->%s\n", ip, filename);
    if (is_daemon) syslog(LOG_INFO, "%s->%s", ip, filename);
}

void sendError(int fd, const struct sockaddr* to, socklen_t to_length,
        int code, char* message) {
    Error error;
    error.op_code = htons(MSG_ERROR);
    error.err_code = code;
    strcpy(error.err_message, message);
    sendto(fd, &error, sizeof(error), 0, to, to_length);
    fprintf(stderr, "TFTP ERROR %d %s\n", code, message);
    if (is_daemon) syslog(LOG_INFO, "TFTP ERROR %d %s", code, message);
}

#define SEND_ERROR(code, message) \
    sendError(socket, (const struct sockaddr*) &from, from_length,\
    code, message)
void listenForRequests(char* service_name, int daemon) {
    int socket;
    Request request;
    struct sockaddr_in from;
    socklen_t from_length = sizeof(from);
    int octets_received;

    ReuseNextSocketAddress();
    socket = UDPServer(NULL, service_name);
    if (daemon) DaemonInit(PNAME, LOG_FTP);
    while (1) {
        octets_received = recvfrom(socket, &request, sizeof(request), 0,
            (struct sockaddr*) &from, &from_length);
        if (octets_received < 0) {
            continue; // Ignore reading errors
        } else if (octets_received < 4) {
            SEND_ERROR(0, "Invalid request");
            continue;
        }

        switch (ntohs(request.op_code)) {
            case MSG_RRQ:
                if (fork() == 0) {
                    close(socket);
                    handleRequest(&request, &from, from_length);
                    exit(0);
                }
                break;
            case MSG_WRQ:
                SEND_ERROR(0, "WRQ not supported");
                break;
            default:
                SEND_ERROR(0, "Invalid op code");
        }
    }
    Close(socket);
}
#undef SEND_ERROR

#define SEND_ERROR(code, message) \
    sendError(socket, (const struct sockaddr*) from, from_length,\
    code, message)
#define NETASCII "netascii"
#define OCTET "octet"
#define WORKING_DIR "/tftpboot/"
void handleRequest(Request* request, struct sockaddr_in* from,
        socklen_t from_length) {
    int socket = Socket(AF_INET, SOCK_DGRAM, 0);
    SetReceiveTimeout(socket, 1, 0);
    char* name = request->name_and_mode;
    char* mode = parseNameAndMode(socket, from, from_length, name,
        sizeof(request->name_and_mode));
    FILE* fd;
    int octet;
    char from_presentation[INET_ADDRSTRLEN];

    InetNtop(AF_INET, &(from->sin_addr), from_presentation,
        INET_ADDRSTRLEN);
    logRequest(from_presentation, name);

    if (strcasecmp(mode, NETASCII) == 0) {
        octet = 0;
    } else if (strcasecmp(mode, OCTET) == 0) {
        octet = 1;
    } else {
        SEND_ERROR(0, "Invalid or unsupported transfer mode");
        Close(socket);
        exit(1);
    }

    if (chdir(WORKING_DIR) != 0) err(1, "chdir() failed");
    if (StartsWith(name, "/") && !StartsWith(name, WORKING_DIR)) {
        SEND_ERROR(2, "Access denied");
        Close(socket);
        exit(1);
    }
    if (!Exists(name)) {
        SEND_ERROR(1, "File not found");
        Close(socket);
        exit(1);
    }
    if (!Readable(name)) {
        SEND_ERROR(2, "Access denied");
        Close(socket);
        exit(1);
    }

    fd = fopen(name, "r");
    if (fd == NULL) {
        SEND_ERROR(0, "File could not be opened");
        Close(socket);
        exit(1);
    }
    transferFile(socket, fd, from, from_length, octet);

    fclose(fd);
    Close(socket);
}

char* parseNameAndMode(int socket, struct sockaddr_in* from,
        socklen_t from_length, char* name_and_mode, int length) {
    char* mode;
    int i;
    for (i = 0; i < length; i ++) {
        if (name_and_mode[i] == '\0') break;
    }

    if (i == 0) {
        SEND_ERROR(0, "Filename not given");
        Close(socket);
        exit(1);
    } else if (i == length) {
        SEND_ERROR(0, "Filename not null-terminated");
        Close(socket);
        exit(1);
    }

    i++;
    mode = &name_and_mode[i];
    for (; i < length; i ++) {
        if (name_and_mode[i] == '\0') break;
    }

    if (mode == &name_and_mode[i]) {
        SEND_ERROR(0, "Mode not null-terminated");
        Close(socket);
        exit(1);
    } else if (i == length) {
        SEND_ERROR(0, "Mode not null-terminated");
        Close(socket);
        exit(1);
    }

    return mode;
}

#define MAX_TRIES 4
void transferFile(int socket, FILE* fd, struct sockaddr_in* from,
        socklen_t from_length, int octet) {
    Data data;
    data.op_code = htons(MSG_DATA);
    Ack ack;
    int length;
    int block_number = 1;
    int last_block = 0;
    int read_length;
    int i;
    int lfPending = 0;
    int current_ack = 0;
    int previous_ack = 0;

    while (1) {
        length = fread(data.data, 1, sizeof(data.data), fd);
        if (!octet) {
            char* pData = data.data;
            for (i = 0; i < length; i ++, pData ++) {
                if (i == 0 && lfPending) {
                    if (length < sizeof(data.data)) {
                        memcpy(pData+1, pData, length);
                        length ++;
                    } else {
                        memcpy(pData+1, pData, length-1);
                        fseek(fd, -1, SEEK_CUR);
                    }
                    *pData = '\n';
                    lfPending = 0;
                    continue;
                }
                if (*pData == '\n') {
                    if (i != 0 && *(pData-1) == '\r') continue;
                    if (i < sizeof(data.data)-1) {
                        if (length < sizeof(data.data)) {
                            memcpy(pData+1, pData, length-i);
                            length ++;
                        } else {
                            memcpy(pData+1, pData, length-i-1);
                            fseek(fd, -1, SEEK_CUR);
                        }
                    } else {
                        lfPending = 1;
                    }
                    *pData = '\r';
                }
            }
        }

        if (length < sizeof(data.data)) last_block = 1;
        data.block_number = htons(block_number);
        for (i = 0; i < MAX_TRIES; i ++) {
            sendto(socket, &data, 4+length, 0,
                (const struct sockaddr*) from, from_length);
            sas: read_length = recvfrom(socket, &ack, sizeof(ack), 0,
                (struct sockaddr*) from, &from_length);
            if (read_length == -1) {
                if (errno == EWOULDBLOCK) {
                    continue;
                } else {
                    // Client disconnected
                    Close(socket);
                    exit(1);
                }
            }
            if (read_length != sizeof(ack)
                    || ntohs(ack.op_code) != MSG_ACK) {
                SEND_ERROR(0, "Only ACK messages allowed during transfer");
                Close(socket);
                exit(1);
            }

            previous_ack = current_ack;
            current_ack = ntohs(ack.block_number);
            if (current_ack == block_number) break;
            if (current_ack == previous_ack) goto sas;
        }
        if (i == MAX_TRIES) {
            Close(socket);
            exit(1);
        }

        if (last_block) break;
        block_number ++;
    }
}

