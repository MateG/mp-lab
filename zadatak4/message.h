#ifndef MESSAGE_H
#define MESSAGE_H

#define MSG_RRQ 1
#define MSG_WRQ 2 // Not implemented
typedef struct {
    uint16_t op_code;
    char name_and_mode[514];
} Request;

#define MSG_DATA 3
typedef struct {
    uint16_t op_code;
    uint16_t block_number;
    char data[512];
} Data;

#define MSG_ACK 4
typedef struct {
    uint16_t op_code;
    uint16_t block_number;
} Ack;

#define MSG_ERROR 5
typedef struct {
    uint16_t op_code;
    uint16_t err_code;
    char err_message[512];
} Error;

#endif
