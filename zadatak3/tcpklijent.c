#include "mrepro.h"
#include "messages.h"

#define USAGE "Usage: %s [-s server] [-p port] [-c] filename\n"

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT "1234"

int RequestAndStoreFile(int socket, const char* filename, int offset);

int main(int argc, char** argv) {
    char* server_ip = DEFAULT_IP;
    char* server_port = DEFAULT_PORT;
    char* filename;
    int ch;
    int option_continue = 0;
    int offset;
    int socket;
    int status;

    while ((ch = getopt(argc, argv, "s:p:c")) != -1) {
        switch (ch) {
            case 's': server_ip = optarg; break;
            case 'p': server_port = optarg; break;
            case 'c': option_continue = 1; break;
            default:
                fprintf(stderr, USAGE, argv[0]);
                return 1;
        }
    }

    if (optind + 1 != argc) {
        fprintf(stderr, USAGE, argv[0]);
        return 1;
    }

    filename = argv[optind];
    socket = TCPClient(server_ip, server_port);
    if (Exists(filename)) {
        if (!option_continue) {
            errx(1, "File already exists, use -c option for appending");
        }
        if (!Writable(filename)) {
            errx(1, "Writing access denied");
        }
        offset = FileSize(filename);
    } else {
        offset = 0;
    }

    status = RequestAndStoreFile(socket, filename, offset);
    Close(socket);
    return status;
}

int RequestAndStoreFile(int socket, const char* filename, int offset) {
    struct ClientMessage buffer_out;
    struct ServerMessage buffer_in;
    FILE* file;
    int read_length;

    buffer_out.offset = htonl(offset);
    strcpy(buffer_out.filename, filename);
    Write(socket, &buffer_out, sizeof(buffer_out));

    read_length = Read(socket, &buffer_in, sizeof(buffer_in));
    if (read_length == 0) return -1;
    read_length -= sizeof(buffer_in.code);

    switch (buffer_in.code) {
        case FILE_OK:
            file = OpenFile(filename, offset == 0 ? "wb" : "ab");
            fwrite(&buffer_in.message, read_length, 1, file);
            while ((read_length = Read(socket, buffer_in.message,
                    sizeof(buffer_in.message))) != 0) {
                fwrite(buffer_in.message, read_length, 1, file);
            }
            CloseFile(file);
            break;
        case FILE_NOT_FOUND:
            fprintf(stderr, "Not found: %s\n", buffer_in.message);
            break;
        case FILE_FORBIDDEN:
            fprintf(stderr, "Forbidden: %s\n", buffer_in.message);
            break;
        case FILE_ERROR:
            fprintf(stderr, "Error: %s\n", buffer_in.message);
            break;
        default:
            fprintf(stderr, "Unknown message: %s (0x%02x)\n",
                buffer_in.message, buffer_in.code);
    }

    return buffer_in.code;
}
