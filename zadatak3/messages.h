#define MAX_FILENAME 255
struct ClientMessage {
    uint32_t offset;
    char filename[MAX_FILENAME];
};
#undef MAX_FILENAME

#define FILE_OK 0x00
#define FILE_NOT_FOUND 0x01
#define FILE_FORBIDDEN 0x02
#define FILE_ERROR 0x03

#define MAX_MESSAGE 8096
struct ServerMessage {
    uint8_t code;
    char message[MAX_MESSAGE];
};
#undef MAX_MESSAGE
