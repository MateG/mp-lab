#include "mrepro.h"
#include "messages.h"

#define USAGE "Usage: %s [-p port]\n"

#define DEFAULT_PORT "1234"
#define BACKLOG 10

void handleRequests(int socket);
void SendError(int fd, struct ServerMessage* buffer,
        uint8_t code, const char* message);

int main(int argc, char** argv) {
    char* port = DEFAULT_PORT;
    int ch;
    int socket;

    while ((ch = getopt(argc, argv, "p:")) != -1) {
        switch (ch) {
            case 'p': port = optarg; break;
            default:
                fprintf(stderr, USAGE, argv[0]);
                return 1;
        }
    }

    if (optind != argc) {
        fprintf(stderr, USAGE, argv[0]);
        return 1;
    }

    socket = TCPServerOnPort(port, BACKLOG);
    handleRequests(socket);
    Close(socket);
    return 0;
}

#define SEND_ERR(code, message) \
    SendError(new_fd, &out_buffer, code, message)
void handleRequests(int socket) {
    int new_fd;
    struct sockaddr_in from;
    socklen_t from_length;
    struct ClientMessage in_buffer;
    struct ServerMessage out_buffer;
    FILE* file;
    int read_length;

    while (1) {
        new_fd = Accept(socket, (struct sockaddr*) &from, &from_length);
        read_length = ReadUntil('\0', sizeof(in_buffer.offset)+1,  new_fd,
            &in_buffer, sizeof(in_buffer));
        if (read_length == 0) {
            Close(new_fd);
            continue;
        }

        if (Contains(in_buffer.filename, "/")) {
            SEND_ERR(FILE_NOT_FOUND, "Working directory files only");
            continue;
        }
        if (!Exists(in_buffer.filename)) {
            SEND_ERR(FILE_NOT_FOUND, "File does not exist");
            continue;
        }
        if (!Readable(in_buffer.filename)) {
            SEND_ERR(FILE_FORBIDDEN, "Reading access denied");
            continue;
        }

        file = fopen(in_buffer.filename, "rb");
        if (file == NULL) {
            SEND_ERR(FILE_ERROR, "File could not be opened");
            continue;
        }
        if (fseek(file, ntohl(in_buffer.offset), SEEK_SET) != 0) {
            SEND_ERR(FILE_ERROR, "Invalid offset");
            CloseFile(file);
            continue;
        }

        out_buffer.code = FILE_OK;
        read_length = fread(out_buffer.message, sizeof(uint8_t),
            sizeof(out_buffer.message), file);
        Write(new_fd, &out_buffer, sizeof(out_buffer.code)+read_length);

        while ((read_length = fread(out_buffer.message, sizeof(uint8_t),
                sizeof(out_buffer.message), file)) != 0) {
            Write(new_fd, out_buffer.message, read_length);
        }

        CloseFile(file);
        Close(new_fd);
    }
}

void SendError(int fd, struct ServerMessage* buffer, uint8_t code,
        const char* message) {
    buffer->code = code;
    strcpy(buffer->message, message);
    Write(fd, buffer, sizeof(*buffer));
    Close(fd);
}
