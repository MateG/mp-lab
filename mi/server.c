#include "mrepro.h"

#define USAGE "Usage: %s [-p tcp_port] [-t timeout] ip1 udp1 {ip2 udp2 ... ip5 udp5}\n"

#define DEFAULT_PORT "1234"
#define DEFAULT_TIMEOUT 20
#define BACKLOG 10

struct sensor {
    char ip[INET_ADDRSTRLEN];
    char port[22];
};

void handleRequests(int socket, int* sensor_sockets, int sensor_count);

int main(int argc, char** argv) {
    char* port = DEFAULT_PORT;
    int timeout = DEFAULT_TIMEOUT;
    int ch;
    int socket;
    struct sensor sensors[5];
    int sensor_count;
    int sensor_sockets[5];
    char* buffer = "INIT\n";
    struct timeval timeout_value;

    while ((ch = getopt(argc, argv, "p:t:")) != -1) {
        switch (ch) {
            case 'p': port = optarg; break;
            case 't': timeout = atoi(optarg); break;
            default:
                fprintf(stderr, USAGE, argv[0]);
                return 1;
        }
    }

    timeout_value.tv_sec = timeout;
    timeout_value.tv_usec = 0;

    sensor_count = (argc-optind) / 2;
    if (((argc - optind) % 2) || sensor_count <= 0 || sensor_count > 5) {
        fprintf(stderr, USAGE, argv[0]);
        return 1;
    }
    for (int i = 0; i < sensor_count; i ++) {
        strcpy(sensors[i].ip, argv[optind + 2*i]);
        strcpy(sensors[i].port, argv[optind + 2*i + 1]);
    }

    for (int i = 0; i < sensor_count; i ++) {
        sensor_sockets[i] = UDPClient(sensors[i].ip, sensors[i].port);
        SetSockOpt(sensor_sockets[i], SOL_SOCKET, SO_RCVTIMEO,
            &timeout_value, sizeof(struct timeval));
        Write(sensor_sockets[i], buffer, strlen(buffer) + 1);
    }

    socket = TCPServerOnPort(port, BACKLOG);
    SetSockOpt(socket, SOL_SOCKET, SO_REUSEADDR, &(int) {1}, sizeof(int));

    handleRequests(socket, sensor_sockets, sensor_count);
    Close(socket);
    return 0;
}

void handleRequests(int socket, int* sensor_sockets, int sensor_count) {
    int new_fd;
    struct sockaddr_in from;
    socklen_t from_length;
    int read_length;
    char buffer[8];
    char from_ip[INET_ADDRSTRLEN];

    char status_response[3];
    char data_response[513];
    int data_index;
    int sensor_value;
    double avg;
    char avg_response[8];

    status_response[0] = sensor_count + '0';
    status_response[1] = '\n';
    status_response[2] = '\0';

    while (1) {
        new_fd = Accept(socket, (struct sockaddr*) &from, &from_length);
        InetNtop(AF_INET, &from.sin_addr.s_addr, from_ip, INET_ADDRSTRLEN);
        fprintf(stderr, "Spojio se: %s:%d\n", from_ip, ntohs(from.sin_port));

        while (1) {
            read_length = ReadUntil('\n', 1, new_fd, buffer, 8);
            if (read_length <= 0) break;
            fprintf(stderr, "Naredba: %s", buffer);

            buffer[read_length-1] = '\0';
            if (strcmp(buffer, "status") == 0) {
                Write(new_fd, status_response, 3);
            } else if (StartsWith(buffer, "data ")) {
                data_index = atoi(&buffer[5]) - 1;
                if (data_index < 0 || data_index >= sensor_count) {
                    Write(new_fd, "ERROR\n", 7);
                    continue;
                }

                Write(sensor_sockets[data_index], "STAT\n", 6);
                Clear(data_response, 513);
                read_length = read(sensor_sockets[data_index], data_response, 513);
                if (read_length == -1) {
                    Write(new_fd, "ERROR\n", 7);
                    continue;
                }

                data_response[read_length] = '\n';
                Write(new_fd, data_response, strlen(data_response)+1);
            } else if (strcmp(buffer, "avg") == 0) {
                avg = 0.0;
                for (int i = 0; i < sensor_count; i ++) {
                    Write(sensor_sockets[i], "STAT\n", 6);
                    Clear(data_response, 513);
                    read_length = read(sensor_sockets[i], data_response, 513);
                    if (read_length == -1) {
                        Write(new_fd, "ERROR\n", 7);
                        continue;
                    }
                    sensor_value = atoi(data_response);
                    avg += sensor_value;
                }
                avg /= sensor_count;

                Clear(avg_response, 8);
                sprintf(avg_response, "%d\n", (int) avg); // integer average value
                Write(new_fd, avg_response, strlen(avg_response)+1);
            } else if (strcmp(buffer, "quit") == 0) {
                break;
            } else {
                Write(new_fd, "ERROR\n", 7); // invalid command
            }
        }
        fprintf(stderr, "Odspojio se: %s:%d\n", from_ip, ntohs(from.sin_port));
        Close(new_fd);
    }
}
