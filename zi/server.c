#include "mrepro.h"

#define USAGE "Usage: %s [-t tcp_port] [-u udp_port] [-s sec] ip1 udp1 {ip2 udp2 ... ip5 udp5}\n"

#define DEFAULT_PORT "1234"
#define DEFAULT_TIMEOUT 5
#define BACKLOG 10

struct sensor {
    char ip[INET_ADDRSTRLEN];
    char port[22];
};

void initSensors();
void handleRequests(int tcp_socket, int udp_socket);
void handleCommand(int fd);

int udp_socket;
int timeout = DEFAULT_TIMEOUT;

int sensor_count;
struct sensor sensors[5];
int sensor_sockets[5];
struct sockaddr_in sensor_addr[5];
int last[5];

int tcp_disconnected = 0;

int main(int argc, char** argv) {
    char* tcp_port = DEFAULT_PORT;
    char* udp_port = DEFAULT_PORT;
    int ch;
    int tcp_socket;

    while ((ch = getopt(argc, argv, "t:u:s:")) != -1) {
        switch (ch) {
            case 't': tcp_port = optarg; break;
            case 'u': udp_port = optarg; break;
            case 's': timeout = atoi(optarg); break;
            default:
                fprintf(stderr, USAGE, argv[0]);
                return 1;
        }
    }

    sensor_count = (argc-optind) / 2;
    if (((argc - optind) % 2) || sensor_count <= 0 || sensor_count > 5) {
        fprintf(stderr, USAGE, argv[0]);
        return 1;
    }

    for (int i = 0; i < sensor_count; i ++) {
        strcpy(sensors[i].ip, argv[optind + 2*i]);
        strcpy(sensors[i].port, argv[optind + 2*i + 1]);
    }

    initSensors();

    ReuseNextSocketAddress();
    tcp_socket = TCPServerOnPort(tcp_port, BACKLOG);
    ReuseNextSocketAddress();
    udp_socket = UDPServerOnPort(udp_port);

    handleRequests(tcp_socket, udp_socket);

    close(tcp_socket);
    close(udp_socket);
    return 0;
}

void initSensors() {
    char* buffer = "INIT\n";

    for (int i = 0; i < sensor_count; i ++) {
        sensor_sockets[i] = Socket(AF_INET, SOCK_DGRAM, 0);
        SetReceiveTimeout(sensor_sockets[i], timeout, 0);
        sensor_addr[i] = GetUDPAddress(sensors[i].ip, sensors[i].port);
        sendto(sensor_sockets[i], buffer, strlen(buffer) + 1, 0,
            (struct sockaddr*) &sensor_addr[i], sizeof(struct sockaddr));
        last[i] = -1;
    }
}

#define STDIN 0
void handleRequests(int tcp_socket, int udp_socket) {
    int new_fd;
    struct sockaddr_in from;
    socklen_t from_length;
    char from_ip[INET_ADDRSTRLEN];
    fd_set read_fds;

    char data_response[513];
    int read_length;
    struct sockaddr addr;
    socklen_t addr_length;

    while (1) {
        FD_ZERO(&read_fds);
        FD_SET(STDIN, &read_fds);
        FD_SET(tcp_socket, &read_fds);
        FD_SET(udp_socket, &read_fds);
        for (int i = 0; i < sensor_count; i ++) {
            FD_SET(sensor_sockets[i], &read_fds);
        }

        SelectBlock(udp_socket+1, &read_fds, NULL, NULL);

        for (int i = 0; i < sensor_count; i ++) {
            if (FD_ISSET(sensor_sockets[i], &read_fds)) {
                Clear(data_response, 513);
                addr_length = sizeof(struct sockaddr);
                read_length = recvfrom(sensor_sockets[i],
                    data_response, 513, 0, &addr, &addr_length);

                if (read_length == -1) {
                    continue;
                }

                last[i] = atoi(data_response);
            }
        }

        if (FD_ISSET(STDIN, &read_fds)) {
            handleCommand(STDIN);
        } else if (FD_ISSET(tcp_socket, &read_fds)) {
            new_fd = Accept(tcp_socket,
                (struct sockaddr*) &from, &from_length);
            InetNtop(AF_INET, &from.sin_addr.s_addr,
                from_ip, INET_ADDRSTRLEN);
            fprintf(stderr, "Spojio se: %s:%d\n",
                from_ip, ntohs(from.sin_port));

            while (1) {
                FD_ZERO(&read_fds);
                FD_SET(STDIN, &read_fds);
                FD_SET(tcp_socket, &read_fds);
                FD_SET(udp_socket, &read_fds);
                for (int i = 0; i < sensor_count; i ++) {
                    FD_SET(sensor_sockets[i], &read_fds);
                }

                SelectBlock(sensor_sockets[sensor_count-1]+1,
                    &read_fds, NULL, NULL);

                for (int i = 0; i < sensor_count; i ++) {
                    if (FD_ISSET(sensor_sockets[i], &read_fds)) {
                        Clear(data_response, 513);
                        addr_length = sizeof(struct sockaddr);
                        read_length = recvfrom(sensor_sockets[i],
                        data_response, 513, 0, &addr, &addr_length);

                        if (read_length == -1) {
                            continue;
                        }

                        last[i] = atoi(data_response);
                    }
                }

                if (FD_ISSET(STDIN, &read_fds)) {
                    handleCommand(STDIN);
                } else if (FD_ISSET(tcp_socket, &read_fds)) {
                    int tmp_fd = Accept(tcp_socket,
                        (struct sockaddr*) &from, &from_length);
                    strcpy(data_response,
                        "Dozvoljen samo jedan TCP klijent.\n");
                    write(tmp_fd, data_response, 513);
                    close(tmp_fd);
                } else if (FD_ISSET(udp_socket, &read_fds)) {
                    handleCommand(udp_socket);
                    connect(udp_socket, NULL, sizeof(struct sockaddr));
                }

                handleCommand(new_fd);
                if (tcp_disconnected) break;
            }

            fprintf(stderr, "Odspojio se: %s:%d\n",
                from_ip, ntohs(from.sin_port));
            close(new_fd);
        } else if (FD_ISSET(udp_socket, &read_fds)) {
            handleCommand(udp_socket);
            connect(udp_socket, NULL, sizeof(struct sockaddr));
        }
    }
}

void handleCommand(int fd) {
    int read_length;
    char buffer[8];

    struct sockaddr addr;
    socklen_t addr_length = sizeof(struct sockaddr);

    char status_response[3];
    char data_response[513];
    int data_index;
    double avg;
    char avg_response[8];

    if (fd == udp_socket) {
        read_length = recvfrom(fd, buffer, 8, 0, &addr, &addr_length);
        connect(fd, &addr, addr_length);
    } else {
        read_length = ReadUntil('\n', 1, fd, buffer, 8);
    }
    if (read_length < 0) {
        return;
        tcp_disconnected = 1;
    }

    fprintf(stderr, "Naredba: %s", buffer);

    buffer[read_length-1] = '\0';
    if (strcmp(buffer, "status") == 0) {
        status_response[0] = sensor_count + '0';
        status_response[1] = '\n';
        status_response[2] = '\0';
        write(fd, status_response, 3);
    } else if (StartsWith(buffer, "data ")) {
        data_index = atoi(&buffer[5]) - 1;
        if (data_index < 0 || data_index >= sensor_count) {
            write(fd, "ERROR\n", 7);
            return;
        }

        sendto(sensor_sockets[data_index], "STAT\n", 6,
            0, (struct sockaddr*) &sensor_addr[data_index],
            sizeof(struct sockaddr));
        Clear(data_response, 513);
        read_length = recvfrom(sensor_sockets[data_index], data_response, 513,
            0, &addr, &addr_length);
        if (read_length == -1) {
            write(fd, "ERROR\n", 7);
            return;
        }

        last[data_index] = atoi(data_response);

        data_response[read_length] = '\n';
        write(fd, data_response, strlen(data_response)+1);
    } else if (StartsWith(buffer, "last ")) {
        data_index = atoi(&buffer[5]) - 1;
        if (data_index < 0 || data_index >= sensor_count) {
            write(fd, "ERROR\n", 7);
            return;
        }

        if (last[data_index] == -1) {
            write(fd, "ERROR\n", 7);
            return;
        }

        sprintf(data_response, "%d\n", last[data_index]);
        write(fd, data_response, strlen(data_response)+1);
    } else if (strcmp(buffer, "avg") == 0) {
        avg = 0.0;
        for (int i = 0; i < sensor_count; i ++) {
            if (last[i] == -1) {
                write(fd, "ERROR\n", 7);
                return;
            }

            avg += last[i];
        }
        avg /= sensor_count;

        Clear(avg_response, 8);
        sprintf(avg_response, "%d\n", (int) avg); // integer average value
        write(fd, avg_response, strlen(avg_response)+1);
    } else if (strcmp(buffer, "reset") == 0) {
        for (int i = 0; i < sensor_count; i ++) {
            close(sensor_sockets[i]);
        }
        initSensors();
    } else {
        write(fd, "ERROR\n", 7); // invalid command
    }
}

