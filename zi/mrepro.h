#ifndef MREPRO_H
#define MREPRO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <syslog.h>
#include <err.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

int is_daemon = 0;

int daemon_init(const char* pname, int facility) {
    pid_t pid;

    pid = fork();
    if (pid < 0) return -1; // Error
    else if (pid) exit(0); // Parent exits

    // The calling process is the session leader
    if (setsid() < 0) return -1; // Error

    signal(SIGHUP, SIG_IGN);

    pid = fork();
    if (pid < 0) return -1; // Error
    else if (pid) exit(0); // Child 1 exits

    chdir("/");

    //for (int i = 0; i < 64; i ++) close(i); // Close file descriptors
    for (int i = 0; i < 3; i ++) close(i);

    open("/dev/null", O_RDONLY); // stdin > /dev/null
    open("/dev/null", O_RDWR); // stdout > /dev/null
    open("/dev/null", O_RDWR); // stderr > /dev/null

    openlog(pname, LOG_PID, facility);
    is_daemon = 1;
    return 0;
}

void DaemonInit(const char* pname, int facility) {
    if (daemon_init(pname, facility) != 0) err(1, "DaemonInit failed");
}

void Select(int fd_count, fd_set* read_set, fd_set* write_set,
        fd_set* except_set, struct timeval* timeout) {
    if (select(fd_count, read_set, write_set, except_set, timeout) == -1)
        err(1, "Select failed");
}

void SelectBlock(int fd_count, fd_set* read_set, fd_set* write_set,
        fd_set* except_set) {
    Select(fd_count, read_set, write_set, except_set, NULL);
}

void SelectNoBlock(int fd_count, fd_set* read_set, fd_set* write_set,
        fd_set* except_set) {
    struct timeval timeout = {0, 0};
    Select(fd_count, read_set, write_set, except_set, &timeout);
}

/**
 * Converts the source presentation format address to destination network
 * format.
 *
 * @param family AF_INET or AF_INET6.
 * @param src Presentation format address.
 * @param dst Network format address.
 */
void InetPton(int family, const char* src, void* dst) {
    if (inet_pton(family, src, dst) != 1) err(1, "InetPton failed");
}

/**
 * Converts the source network format address to destination presentation
 * format.
 *
 * @param family AF_INET or AF_INET6.
 * @param src Network format address.
 * @param dst Presentation format address.
 * @param size INET_ADDRSTRLEN or INET6_ADDRSTRLEN.
 * @return Destination presentation format address.
 */
const char* InetNtop(int family, const void* src, char* dst, socklen_t size) {
    const char* presentation = inet_ntop(family, src, dst, size);
    if (presentation == NULL) err(1, "InetNtop failed");
    return presentation;
}

/**
 * Returns a linked list of addresses corresponding to the specified
 * parameters.
 *
 * @param host_name Valid host name, numeric address or NULL.
 * @param service_name Valid service name, port number or NULL.
 * @param hints Structure specifying additional options.
 * @param result Resulting linked list.
 */
void GetAddrInfo(const char* host_name, const char* service_name,
        const struct addrinfo* hints, struct addrinfo** result) {
    if (getaddrinfo(host_name, service_name, hints, result) != 0)
        err(1, "GetAddrInfo failed");
}

/**
 * Gets the specified socket option.
 *
 * @param fd Socket descriptor.
 * @param level SOL_SOCKET, IPPROTO_IP, IPPROTO_TCP or something else.
 * @param name See getsockopt(2), ip(4), tcp(4),...
 * @param value Return value.
 * @param length Size of the return value.
 */
void GetSockOpt(int fd, int level, int name, void* value,
        socklen_t* length) {
    if (getsockopt(fd, level, name, value, length) != 0)
        err(1, "GetSockOpt failed");
}

/**
 * Updates the specified socket option.
 *
 * @param fd Socket descriptor.
 * @param level SOL_SOCKET, IPPROTO_IP, IPPROTO_TCP or something else.
 * @param name See getsockopt(2), ip(4), tcp(4),...
 * @param value The new value.
 * @param length Length of the new value.
 */
void SetSockOpt(int fd, int level, int name, const void* value,
        socklen_t length) {
    if (setsockopt(fd, level, name, value, length) != 0)
        err(1, "SetSockOpt failed");
}

void SetReceiveTimeout(int fd, int seconds, int microseconds) {
    struct timeval timeout = {seconds, microseconds};
    SetSockOpt(fd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
}

static int socket_reuse_address = 0;

void ReuseNextSocketAddress() {
    socket_reuse_address = 1;
}

/**
 * Allocates a socket and returns its descriptor.
 *
 * @param family PF_INET, PF_INET6 or something else.
 * @param type SOCK_STREAM, SOCK_DGRAM or something else.
 * @param protocol Specific protocol code or 0 for default.
 * @return The socket descriptor.
 */
int Socket(int family, int type, int protocol) {
    int fd = socket(family, type, protocol);
    if (fd < 0) err(1, "Socket could not be created");
    if (socket_reuse_address) {
        SetSockOpt(fd, SOL_SOCKET, SO_REUSEADDR, &(int) {1}, sizeof(int));
        socket_reuse_address = 0;
    }
    return fd;
}

/**
 * Attempts a 3-way handshake with the host at the given address (for TCP) or
 * links the given address to the specified socket for easier communication
 * (for UDP).
 *
 * @param fd Socket descriptor.
 * @param address The given address.
 * @param address_length Length of the given address.
 */
void Connect(int fd, const struct sockaddr* address,
        socklen_t address_length) {
    if (connect(fd, address, address_length) != 0) err(1, "Connect failed");
}

/**
 * Binds the specified socket to the given (local) address.
 *
 * @param fd Socket descriptor.
 * @param address The given address.
 * @param address_length Length of the given address.
 */
void Bind(int fd, const struct sockaddr* address, int address_length) {
    if (bind(fd, address, address_length) != 0) err(1, "Bind failed");
}

/**
 * Starts to listen for incoming connections.
 *
 * @param fd Socket descriptor.
 * @param backlog The maximum length of the pending connection queue.
 */
void Listen(int fd, int backlog) {
    if (listen(fd, backlog) != 0) err(1, "Listen failed");
}

/**
 * Closes the given descriptor.
 *
 * @param fd The given descriptor.
 */
void Close(int fd) {
    if (close(fd) != 0) err(1, "Close failed");
}

/**
 * Accepts an incoming connection from the pending connection queue.
 *
 * @param fd Passive mode socket descriptor.
 * @param from Memory allocated for the client address (or NULL).
 * @param from_length Pointer to the client address size (or NULL).
 * @return New client connection socket descriptor.
 */
int Accept(int fd, struct sockaddr* from, socklen_t* from_length) {
    int new_fd = accept(fd, from, from_length);
    if (fd == -1) err(1, "Accept failed");
    return new_fd;
}

/**
 * Transmits the specified message to the specified address using the
 * specified socket.
 *
 * @param fd Socket descriptor.
 * @param buffer The specified message.
 * @param length Length of the specified message.
 * @param to The specified address.
 * @param to_length Length of the specified address.
 * @return Number of sent octets.
 */
int SendTo(int fd, const void* buffer, size_t length,
        const struct sockaddr* to, socklen_t to_length) {
    int octets_sent = sendto(fd, buffer, length, 0, to, to_length);
    if (octets_sent < 0) err(1, "SendTo failed");
    return octets_sent;
}

/**
 * Writes the specified message using the specified descriptor.
 *
 * @param fd The specified descriptor.
 * @param buffer The specified message.
 * @param length Length of the specified message.
 * @return Number of written octets.
 */
int Write(int fd, const void* buffer, size_t length) {
    int octets_written = write(fd, buffer, length);
    if (octets_written < 0) err(1, "Write failed");
    return octets_written;
}

/**
 * Receives a message from the specified address using the specified socket.
 *
 * @param fd Socket descriptor.
 * @param buffer Message buffer.
 * @param buffer_length Length of the message buffer.
 * @param from The specified address.
 * @param from_length Length of the specified address.
 * @return Number of received octets.
 */
int RecvFrom(int fd, void* buffer, size_t buffer_length,
        struct sockaddr* from, socklen_t* from_length) {
    int octets_received = recvfrom(fd, buffer, buffer_length, 0,
        from, from_length);
    if (octets_received < 0) err(1, "RecvFrom failed");
    return octets_received;
}

/**
 * Reads a message using the specified descriptor.
 *
 * @param fd The specified descriptor.
 * @param buffer Message buffer.
 * @param buffer_length Length of the message buffer.
 * @return Number of read octets.
 */
int Read(int fd, void* buffer, size_t buffer_length) {
    int octets_read = read(fd, buffer, buffer_length);
    if (octets_read < 0) err(1, "Read failed");
    return octets_read;
}

/**
 * Reads a message using the specified descriptor until the specified
 * terminating character is read and at least minimum number of octets have
 * been read. Note that the function call may perform extremely slow with
 * relatively long messages.
 *
 * @param ch The specified terminating character.
 * @param min The minimum number of octets to read.
 * @param fd Socket descriptor.
 * @param buffer Message buffer.
 * @param buffer_length Length of the message buffer.
 * @return Number of read octets.
 */
int ReadUntil(char ch, int min, int fd, void* buffer,
        size_t buffer_length) {
    int read_length, read_total;
    char* str = buffer;
    char read_ch;

    read_total = 0;
    while (1) {
        read_length = read(fd, &read_ch, 1);
        if (read_length < 0) {
            return -1;
        } else if (read_length == 0) {
            if (read_total == 0) return 0;
            else break;
        } else {
            if (read_total < buffer_length - 1) {
                read_total ++;
                *str ++ = read_ch;
            }
            if (read_total >= min && read_ch == ch) break;
        }
    }
    *str = '\0';
    return read_total;
}

/**
 * Clears the given memory.
 *
 * @param memory The given memory.
 * @param size Size of the given memory.
 */
void Clear(void* memory, size_t size) {
    memset(memory, 0, size);
}

/**
 * Checks if the given string starts with the given prefix.
 *
 * @param str The given string.
 * @param prefix The given prefix.
 * @return 1 if true, 0 if false.
 */
int StartsWith(const char* str, const char* prefix) {
    size_t prefix_length = strlen(prefix);
    return strlen(str) < prefix_length ?
        0 : strncmp(prefix, str, prefix_length) == 0;
}

/**
 * Checks if the given big string contains the given little string.
 *
 * @param big The given big string.
 * @param little The given little string.
 * @return 1 if true, 0 if false.
 */
int Contains(const char* big, const char* little) {
    return strstr(big, little) != NULL;
}

/**
 * Counts the occurrences of the given character in the given string.
 *
 * @param string The given string.
 * @param ch The given character.
 * @return The occurrence count.
 */
int CountOccurrences(char* string, char ch) {
    int count = 0;
    char* c = string;
    while (*c != '\0') {
        if (*c == ch) count ++;
        c ++;
    }
    return count;
}

/**
 * Splits the given string using the given delimiter.
 *
 * @param original The given string.
 * @param delimiter The given delimiter.
 * @param length The number of resulting parts.
 * @result Pointers to the resulting parts.
 */
char** Split(char* original, char delimiter, int* length) {
    char** result;
    char* part;
    char* heap_token;
    int counter = 0;
    char delimiter_string[2];
    delimiter_string[0] = delimiter;
    delimiter_string[1] = '\0';

    *length = CountOccurrences(original, delimiter) + 1;
    result = (char**) malloc((*length) * sizeof(char*));

    part = strtok(original, delimiter_string);
    while (part != NULL) {
        heap_token = (char *) malloc((strlen(part)+1) * sizeof(char));
        strcpy(heap_token, part);
        result[counter++] = heap_token;
        part = strtok(NULL, delimiter_string);
    }
    return result;
}

/**
 * Checks the accessibility of the file specified by the given filepath.
 *
 * @param filepath The given filepath.
 * @param mode R_OK, W_OK, X_OK, F_OK or some bitwise-inclusive OR operation.
 * @return 1 if true, 0 if false.
 */
int Access(const char* filepath, int mode) {
    return access(filepath, mode) == 0 ? 1 : 0;
}

/**
 * Checks the existence of the file specified by the given filepath.
 *
 * @param filepath The given filepath.
 * @return 1 if true, 0 if false.
 */
int Exists(const char* filepath) {
    return Access(filepath, F_OK);
}

/**
 * Checks the readability of the file specified by the given filepath.
 *
 * @param filepath The given filepath.
 * @return 1 if true, 0 if false.
 */
int Readable(const char* filepath) {
    return Access(filepath, R_OK);
}

/**
 * Checks the writability of the file specified by the given filepath.
 *
 * @param filepath The given filepath.
 * @return 1 if true, 0 if false.
 */
int Writable(const char* filepath) {
    return Access(filepath, W_OK);
}

/**
 * Returns the size of the file specified by the given filepath.
 *
 * @param filepath The given filepath.
 * @return The file size in bytes.
 */
int FileSize(const char* filepath) {
    struct stat status;
    if (stat(filepath, &status) != 0) err(1, "FileSize failed");
    return status.st_size;
}

/**
 * Opens the file specified by the given filepath using the given mode and
 * associates a stream with it.
 *
 * @param filepath The given filepath.
 * @param mode The given mode.
 * @return The FILE pointer.
 */
FILE* OpenFile(const char* filepath, const char* mode) {
    FILE* file = fopen(filepath, mode);
    if (file == NULL) err(1, "OpenFile failed");
    return file;
}

/**
 * Closes the specified file.
 *
 * @param file The FILE pointer.
 */
void CloseFile(FILE* file) {
    if (fclose(file) != 0) err(1, "CloseFile failed");
}

/**
 * Returns the current file position indicator value for the specified stream.
 *
 * @param file The FILE pointer.
 * @return The current position.
 */
long FilePosition(FILE* file) {
    long position = ftell(file);
    if (position == -1) err(1, "FilePosition failed");
    return position;
}

/**
 * Returns the corresponding network format IPv4 address.
 *
 * @param host_name Valid host name, numeric address or NULL.
 * @param service_name Valid service name, port number or NULL.
 * @param socktype SOCK_STREAM, SOCK_DGRAM or something else.
 * @param flags Optional flags (e.g. AI_PASSIVE).
 * @return The network format address.
 */
struct sockaddr_in GetIPv4Address(const char* host_name,
        const char* service_name, int socktype, int flags) {
    struct addrinfo hints;
    struct addrinfo* results;
    struct sockaddr_in address;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = socktype;
    hints.ai_flags |= flags;
    GetAddrInfo(host_name, service_name, &hints, &results);
    memcpy(&address, results->ai_addr, sizeof(address));
    freeaddrinfo(results);
    return address;
}

/**
 * Returns the corresponding passive network format IPv4 address.
 *
 * @param service_name Valid service name, port number or NULL.
 * @param socktype SOCK_STREAM, SOCK_DGRAM or something else.
 * @return The network format address.
 */
struct sockaddr_in GetPassiveIPv4Address(const char* service_name,
        int socktype) {
    struct sockaddr_in address;
    address = GetIPv4Address(NULL, service_name, socktype, AI_PASSIVE);
    address.sin_addr.s_addr = INADDR_ANY;
    return address;
}

/**
 * Returns the corresponding network format IPv4 UDP address.
 *
 * @param host_name Valid host name, numeric address or NULL.
 * @param service_name Valid service name, port number or NULL.
 * @return The network format address.
 */
struct sockaddr_in GetUDPAddress(const char* host_name,
        const char* service_name) {
    return GetIPv4Address(host_name, service_name, SOCK_DGRAM, 0);
}

/**
 * Returns the corresponding passive network format IPv4 UDP address.
 *
 * @param service_name Valid service name, port number or NULL.
 * @return The network format address.
 */
struct sockaddr_in GetPassiveUDPAddress(const char* service_name) {
    return GetPassiveIPv4Address(service_name, SOCK_DGRAM);
}

/**
 * Returns the corresponding network format IPv4 TCP address.
 *
 * @param host_name Valid host name, numeric address or NULL.
 * @param service_name Valid service name, port number or NULL.
 * @return The network format address.
 */
struct sockaddr_in GetTCPAddress(const char* host_name,
        const char* service_name) {
    return GetIPv4Address(host_name, service_name, SOCK_STREAM, 0);
}

/**
 * Returns the corresponding passive network format IPv4 TCP address.
 *
 * @param service_name Valid service name, port number or NULL.
 * @return The network format address.
 */
struct sockaddr_in GetPassiveTCPAddress(const char* service_name) {
    return GetPassiveIPv4Address(service_name, SOCK_STREAM);
}

/**
 * Reconnects the specified socket to a new address.
 *
 * @param fd Socket descriptor.
 * @param host_name Host name of the new address.
 * @param service_name Service name of the new address.
 */
void ReconnectUDPSocket(int fd, const char* host_name,
        const char* service_name) {
    struct sockaddr_in address = GetUDPAddress(host_name, service_name);
    Connect(fd, (const struct sockaddr*) &address, sizeof(address));
}

/**
 * Allocates and binds a UDP server socket.
 *
 * @param host_name Valid host name, numeric address or NULL (passive).
 * @param service_name Valid service name, port number or NULL.
 * @return The socket descriptor.
 */
int UDPServer(const char* host_name, const char* service_name) {
    int socket;
    struct sockaddr_in address;
    socket = Socket(AF_INET, SOCK_DGRAM, 0);
    address = host_name == NULL ?
        GetPassiveUDPAddress(service_name)
        : GetUDPAddress(host_name, service_name);
    Bind(socket, (const struct sockaddr*) &address, sizeof(address));
    return socket;
}

/**
 * Allocates and binds a passive UDP server socket.
 *
 * @param service_name Valid service name, port number or NULL.
 * @return The socket descriptor.
 */
int UDPServerOnPort(const char* service_name) {
    return UDPServer(NULL, service_name);
}

/**
 * Allocates a UDP client socket and connects it to the specified server.
 *
 * @param host_name Valid host name or numeric address.
 * @param service_name Valid service name or port number.
 * @return The socket descriptor.
 */
int UDPClient(const char* host_name, const char* service_name) {
    int socket;
    struct sockaddr_in address;
    socket = Socket(AF_INET, SOCK_DGRAM, 0);
    address = GetUDPAddress(host_name, service_name);
    Connect(socket, (const struct sockaddr*) &address, sizeof(address));
    return socket;
}

/**
 * Allocates and binds a TCP server socket.
 *
 * @param host_name Valid host name, numeric address or NULL (passive).
 * @param service_name Valid service name, port number or NULL.
 * @param backlog The maximum length of the pending connection queue.
 * @return The socket descriptor.
 */
int TCPServer(const char* host_name, const char* service_name, int backlog) {
    int socket;
    struct sockaddr_in address;
    socket = Socket(AF_INET, SOCK_STREAM, 0);
    address = host_name == NULL ?
        GetPassiveTCPAddress(service_name)
        : GetTCPAddress(host_name, service_name);
    Bind(socket, (const struct sockaddr*) &address, sizeof(address));
    Listen(socket, backlog);
    return socket;
}

/**
 * Allocates and binds a passive TCP server socket.
 *
 * @param service_name Valid service name, port number or NULL.
 * @param backlog The maximum length of the pending connection queue.
 * @return The socket descriptor.
 */
int TCPServerOnPort(const char* service_name, int backlog) {
    return TCPServer(NULL, service_name, backlog);
}

/**
 * Allocates a TCP client socket and connects it to the specified server.
 *
 * @param host_name Valid host name or numeric address.
 * @param service_name Valid service name or port number.
 * @return The socket descriptor.
 */
int TCPClient(const char* host_name, const char* service_name) {
    int socket;
    struct sockaddr_in address;
    socket = Socket(AF_INET, SOCK_STREAM, 0);
    address = GetTCPAddress(host_name, service_name);
    Connect(socket, (struct sockaddr*) &address, sizeof(address));
    return socket;
}

#endif
