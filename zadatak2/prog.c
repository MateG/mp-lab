#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define USAGE \
    "prog [-r] [-t|-u] [-x] [-h|-n] [-46] {hostname | IP_address} {servicename | port}"

#define OPTION_R 0x10
#define OPTION_T 0x08
#define OPTION_U ~OPTION_T
#define OPTION_X 0x04
#define OPTION_H 0x02
#define OPTION_N ~OPTION_H
#define OPTION_4 0x01
#define OPTION_6 ~OPTION_4
#define OPTIONS_DEFAULT (OPTION_T | OPTION_H | OPTION_4)

void regularLookup(char* host_name, char* service_name, char options);
void reverseLookup(char* ip_address, char* port, char options);
void printResults4(struct addrinfo* result, char options);
void printResults6(struct addrinfo* result, char options);

int main(int argc, char** argv)
{
    char ch, options = OPTIONS_DEFAULT, *host_name, *service_name;

    if (argc < 3) errx(1, "%s", USAGE);
    while ((ch = getopt(argc, argv, "rtuxhn46")) != -1) {
        switch (ch) {
            case 'r': options |= OPTION_R; break;
            case 't': options |= OPTION_T; break;
            case 'u': options &= OPTION_U; break;
            case 'x': options |= OPTION_X; break;
            case 'h': options |= OPTION_H; break;
            case 'n': options &= OPTION_N; break;
            case '4': options |= OPTION_4; break;
            case '6': options &= OPTION_6; break;
            default: errx(1, "%s", USAGE);
        }
    }

    if (optind + 2 != argc) errx(1, "%s", USAGE);
    host_name = argv[optind];
    service_name = argv[optind+1];

    if (options & OPTION_R) reverseLookup(host_name, service_name, options);
    else regularLookup(host_name, service_name, options);

    return 0;
}

void regularLookup(char* host_name, char* service_name, char options) {
    struct addrinfo hints, *result, *head;
    int error;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = (options & OPTION_4) ? AF_INET : AF_INET6;
    hints.ai_socktype = (options & OPTION_T) ? SOCK_STREAM : SOCK_DGRAM;
    hints.ai_flags |= AI_CANONNAME;

    error = getaddrinfo(host_name, service_name, &hints, &result);
    if (error) errx(1, "%s", gai_strerror(error));

    head = result;
    if (options & OPTION_4) printResults4(result, options);
    else printResults6(result, options);
    freeaddrinfo(head);
}

void reverseLookup(char* ip_address, char* port, char options) {
    int error, flags = NI_NAMEREQD;
    char host_name[NI_MAXHOST];
    char service_name[NI_MAXSERV];

    if (!(options & OPTION_T)) flags |= NI_DGRAM;
    if (options & OPTION_4) {
        struct sockaddr_in sa;
        sa.sin_family = AF_INET;
        sa.sin_port = htons(atoi(port));
        if (inet_pton(AF_INET, ip_address, &(sa.sin_addr)) != 1) {
            errx(1, "%s is not a valid IPv4 address.", ip_address);
        }
        error = getnameinfo(
            (struct sockaddr*) &sa, sizeof(sa),
            host_name, NI_MAXHOST, service_name, NI_MAXSERV,
            flags
        );
    } else {
        struct sockaddr_in6 sa6;
        sa6.sin6_family = AF_INET6;
        sa6.sin6_port = htons(atoi(port));
        if (inet_pton(AF_INET6, ip_address, &(sa6.sin6_addr)) != 1) {
            errx(1, "%s is not a valid IPv6", ip_address);
        }
        error = getnameinfo(
            (struct sockaddr*) &sa6, sizeof(sa6),
            host_name, NI_MAXHOST, service_name, NI_MAXSERV,
            flags
        );
    }
    if (error) errx(1, "%s", gai_strerror(error));

    printf("%s (%s) %s\n", ip_address, host_name, service_name);
}

void printResults4(struct addrinfo* result, char options) {
    struct sockaddr_in* address;
    in_port_t port;
    char addrstr[NI_MAXHOST];
    while (result) {
        address = (struct sockaddr_in*) result->ai_addr;
        inet_ntop(
            result->ai_family,
            &address->sin_addr,
            addrstr, NI_MAXHOST
        );
        printf("%s (%s) ", addrstr, result->ai_canonname);
        port = address->sin_port;
        if (options & OPTION_H) port = ntohs(port);
        printf((options & OPTION_X) ? "%04x\n" : "%d\n", port);
        result = result->ai_next;
    }
    free(address);
}

void printResults6(struct addrinfo* result, char options) {
    struct sockaddr_in6* address;
    in_port_t port;
    char addrstr[100];
    while (result) {
        address = (struct sockaddr_in6*) result->ai_addr;
        inet_ntop(
            result->ai_family,
            &address->sin6_addr,
            addrstr,
            100
        );
        printf("%s (%s) ", addrstr, result->ai_canonname);
        port = address->sin6_port;
        if (options & OPTION_H) port = ntohs(port);
        printf((options & OPTION_X) ? "%04x\n" : "%d\n", port);
        result = result->ai_next;
    }
    free(address);
}
