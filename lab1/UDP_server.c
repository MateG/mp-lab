#include <stdlib.h>
#include <stdio.h>
#include "mrepro.h"

#define DEFAULT_PORT "1234"
#define DEFAULT_PAYLOAD ""
#define HELLO "HELLO"
#define PAYLOAD "PAYLOAD:"
#define MAX_MESSAGE 512

void handleRequests(int socket, char* payload);

int main(int argc, char** argv) {
    char* port = DEFAULT_PORT;
    char* payload = DEFAULT_PAYLOAD;
    int ch;
    int socket;

    while ((ch = getopt(argc, argv, "l:p:")) != -1) {
        switch (ch) {
            case 'l': port = optarg; break;
            case 'p': payload = optarg; break;
            default:
                errx(1, "Usage: %s -l port -p payload", argv[0]);
                return 1;
        }
    }

    socket = BindedUDPSocket(NULL, port);
    handleRequests(socket, payload);
    Close(socket);
    return 0;
}

void handleRequests(int socket, char* payload) {
    struct sockaddr_in client_address;
    size_t client_address_length;
    char received[MAX_MESSAGE];
    char message[MAX_MESSAGE];

    while (1) {
        RecvFrom(socket, &received, MAX_MESSAGE,
            (struct sockaddr*) &client_address, &client_address_length);
        if (strcmp(received, HELLO) == 0) {
            strcpy(message, PAYLOAD);
            strcat(message, payload);
            printf("Responding with: %s\n", message);
            SendTo(socket, message, strlen(message) + 1,
                (struct sockaddr*) &client_address, sizeof(client_address));
        } else {
            printf("Received invalid message from client: %s\n", received);
        }
    }
}
