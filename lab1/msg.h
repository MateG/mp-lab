#ifndef MSG_H
#define MSG_H

#define MSG_PORT_LENGTH 22 // Service name allowed
#define MSG_PAIR_LENGTH (INET_ADDRSTRLEN + MSG_PORT_LENGTH)
struct MSG_pair {
    char ip[INET_ADDRSTRLEN];
    char port[MSG_PORT_LENGTH];
};

#define MSG_PAIRS 20
struct MSG {
    char command;
    struct MSG_pair pairs[MSG_PAIRS];
};

#define CMD_PROG '0'
#define CMD_RUN  '1'
#define CMD_QUIT '2'

#endif
