#ifndef MREPRO_H
#define MREPRO_H

#include <string.h>
#include <unistd.h>
#include <err.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

void InetPton(int family, const char* src, void* dst) {
    if (inet_pton(AF_INET, src, dst) != 1) err(1, "InetPton failed");
}

void GetAddrInfo(const char* host_name, const char* service_name,
        const struct addrinfo* hints, struct addrinfo** result) {
    if (getaddrinfo(host_name, service_name, hints, result) != 0)
        err(1, "GetAddrInfo failed");
}

int Socket(int family, int type, int protocol) {
    int fd = socket(family, type, protocol);
    if (fd < 0) err(1, "Socket could not be created");
    return fd;
}

void Connect(int fd, const struct sockaddr* address,
        socklen_t address_length) {
    if (connect(fd, address, address_length) != 0) err(1, "Connect failed");
}

void Bind(int fd, const struct sockaddr* address, int address_length) {
    if (bind(fd, address, address_length) != 0) err(1, "Bind failed");
}

void Close(int fd) {
    if (close(fd) != 0) err(1, "Close failed");
}

int SendTo(int fd, const void* buffer, size_t length,
        const struct sockaddr* to, socklen_t to_length) {
    int octets_sent = sendto(fd, buffer, length, 0, to, to_length);
    if (octets_sent < 0) err(1, "SendTo failed");
    return octets_sent;
}

int Write(int fd, const void* buffer, size_t length) {
    int octets_written = write(fd, buffer, length);
    if (octets_written < 0) err(1, "Write failed");
    return octets_written;
}

int RecvFrom(int fd, void* buffer, size_t buffer_length,
        struct sockaddr* from, socklen_t* from_length) {
    int octets_received = recvfrom(fd, buffer, buffer_length, 0,
        from, from_length);
    if (octets_received < 0) err(1, "RecvFrom failed");
    return octets_received;
}

int Read(int fd, void* buffer, size_t buffer_length) {
    int octets_read = read(fd, buffer, buffer_length);
    if (octets_read < 0) err(1, "Read failed");
    return octets_read;
}

int StartsWith(const char* str, const char* prefix) {
    size_t prefix_length = strlen(prefix);
    return strlen(str) < prefix_length ?
        0 : strncmp(prefix, str, prefix_length) == 0;
}

struct sockaddr_in GetIPv4Address(const char* host_name,
        const char* service_name, int socktype) {
    struct addrinfo hints;
    struct addrinfo* results;
    struct sockaddr_in address;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = socktype;
    GetAddrInfo(host_name, service_name, &hints, &results);
    memcpy(&address, results->ai_addr, sizeof(address));
    freeaddrinfo(results);
    return address;
}

struct sockaddr_in GetUDPAddress(const char* host_name,
        const char* service_name) {
    return GetIPv4Address(host_name, service_name, SOCK_DGRAM);
}

int ConnectedUDPSocket(const char* host_name, const char* service_name) {
    int socket;
    struct sockaddr_in address;
    socket = Socket(AF_INET, SOCK_DGRAM, 0);
    address = GetUDPAddress(host_name, service_name);
    Connect(socket, (const struct sockaddr*) &address, sizeof(address));
    return socket;
}

void ReconnectUDPSocket(int fd, const char* host_name,
        const char* service_name) {
    struct sockaddr_in address = GetUDPAddress(host_name, service_name);
    Connect(fd, (const struct sockaddr*) &address, sizeof(address));
}

int BindedUDPSocket(const char* host_name, const char* service_name) {
    int socket;
    struct sockaddr_in address;
    socket = Socket(AF_INET, SOCK_DGRAM, 0);
    address = GetUDPAddress(host_name, service_name);
    if (host_name == NULL) {
        address.sin_addr.s_addr = INADDR_ANY;
    }
    Bind(socket, (const struct sockaddr*) &address, sizeof(address));
    return socket;
}

#endif
